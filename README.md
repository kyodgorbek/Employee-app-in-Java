# Employee-app-in-Java



import javax.swing.JOptionPane;

public class EmployeeApp {
 
 // methods
 private static double readDouble(String prompt){
  String numStr = JOptionPane.showInputDialog(prompt);
   return Double.parseDouble(numStr);
 }
 
 public static void main(String[] args){
  // Read and store the payroll data in an Employee object
  Employee programmer = new Employee(JOptionPane.showInputDialog("Enter employee id"), readDouble("Enter hours worked"), readDouble("Enter hourly rate"));
  
  // Comput gross pay
  double gross = programmer.computeGross();
  
  // Compute net pay given gross pay
  double net = programmer.computeNet(gross);
  
  // Display the Employee object's state abd pay amounts
  System.out.println(programmer.toString());
  System.out.println("Gross pay is $ " + gross);
  System.out.println("Net pay is $ " + net);
 }
}    
